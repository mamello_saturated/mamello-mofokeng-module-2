void main () {
    var student = Student("Mamello", "Netflix", "Johannesburg");
    student.displayInfo();
}

class Student{
   String name, app, city;
   Student (this.name, this.app, this.city);
   
   void displayInfo(){
      print("My name is ${this.name}, my favorite app is ${this.app} and my city is ${this.city}.");
   }
}